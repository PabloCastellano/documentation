# Configurations

## PeerTube configuration

PeerTube configuration is loaded with [node-config](https://www.npmjs.com/package/config). You can have multiple config files that will be selected due to a specific [file load order](https://github.com/lorenwest/node-config/wiki/Configuration-Files#file-load-order).

The configuration file is parsed during application start, which means that PeerTube has to be restarted for the changes to have effect.

You can find the configuration options documented in the [default.yaml](https://github.com/Chocobozzz/PeerTube/blob/develop/config/default.yaml).


## Environment variables

 * `PT_INITIAL_ROOT_PASSWORD`: Set up an initial administrator password. It must be 6 characters or more
 * `FFMPEG_PATH` and `FFPROBE_PATH`: Use custom FFmpeg/FFprobe binaries
 * `HTTP_PROXY` and `HTTPS_PROXY`: (**PeerTube >= 3.4**) Use proxy for HTTP requests
 * `YOUTUBE_DL_DOWNLOAD_HOST`: Use a custom host to download youtube-dl binaries (default is `https://yt-dl.org/downloads/latest/youtube-dl`). Should respect `youtube-dl` URL behaviour:
   * `YOUTUBE_DL_DOWNLOAD_HOST` should redirect (`302`) to another URL that should ends with `/:date/youtube-dl` (for example: `https://example.com/youtube-dl/downloads/2021.06.06/youtube-dl`)
   * This new URL should point to the `youtube-dl` binary (redirections are supported)


## Security

Installing PeerTube following the production guide should be secure enough by default. We list here suggestions
to tighten the security of some parts of PeerTube.

#### Set up a HTTP proxy

With ActivityPub federation and import features, PeerTube does many HTTP requests to the external world.
To prevent private network/URL access, we encourage to use a HTTP proxy using `HTTP_PROXY` and `HTTPS_PROXY` environment variables.

#### Systemd Unit with reduced privileges

A systemd unit template is provided at `support/systemd/peertube.service`. Some directives can be changed to improve security!

 * `PrivateDevices=true`: sets up a new `/dev` mount for the Peertube process and
only adds API pseudo devices like `/dev/null`, `/dev/zero`, or `/dev/random`
but not physical devices. **This won't work on Raspberry Pi. That's why we don't enable it by default**

 * `ProtectHome=true`: sandboxes Peertube such that the service can not access the
 `/home`, `/root`, and `/run/user` folders. If your local Peertube user has its
  home folder in one of the restricted places, either change the home directory
  of the user or set this option to `false`
